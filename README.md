ThatPDF
=======

ThatPDF is an open source application for reading and annotating pdfs

Much of it is built off the excellent iOS PDF Viewer at https://github.com/vfr/Reader
